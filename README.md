# Advent of Code 2022

My repo for the [Advent of Code 2022](https://adventofcode.com/).

This repo uses [Cargo AOC](https://lib.rs/crates/cargo-aoc).
You can install it with `cargo install cargo-aoc`.

And then you can run the project with `cargo aoc`.
