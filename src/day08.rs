#[aoc_generator(day8)]
pub fn generator(input: &str) -> Vec<Vec<u32>> {
    input
        .lines()
        .map(|line| {
            line.chars()
                .map(|char| char.to_digit(10).unwrap())
                .collect()
        })
        .collect()
}

pub fn is_visible(tree_x: usize, tree_y: usize, forest: &Vec<Vec<u32>>) -> bool {
    let tree = forest[tree_y][tree_x];
    let x_size = forest[0].len();
    let y_size = forest.len();

    let is_on_x_edge = tree_x == 0 || tree_x == x_size - 1;
    let is_on_y_edge = tree_y == 0 || tree_y == y_size - 1;

    if is_on_x_edge || is_on_y_edge {
        return true;
    }

    let mut is_visible_top = true;
    let mut is_visible_bottom = true;
    let mut is_visible_left = true;
    let mut is_visible_right = true;

    // Check above
    for y in 0..tree_y {
        if forest[y][tree_x] >= tree {
            is_visible_top = false;
            break;
        }
    }

    for y in tree_y + 1..y_size {
        if forest[y][tree_x] >= tree {
            is_visible_bottom = false;
            break;
        }
    }

    for x in 0..tree_x {
        if forest[tree_y][x] >= tree {
            is_visible_left = false;
            break;
        }
    }

    for x in tree_x + 1..x_size {
        if forest[tree_y][x] >= tree {
            is_visible_right = false;
            break;
        }
    }

    is_visible_bottom || is_visible_top || is_visible_left || is_visible_right
}

// input is [y][x]
#[aoc(day8, part1)]
pub fn part1(input: &Vec<Vec<u32>>) -> u32 {
    let mut sum = 0;

    for y in 0..input.len() {
        for x in 0..input[0].len() {
            if is_visible(x, y, input) {
                sum += 1;
            }
        }
    }

    sum
}

pub fn get_scenic_score(tree_x: usize, tree_y: usize, forest: &Vec<Vec<u32>>) -> i32 {
    let tree = forest[tree_y][tree_x];
    let x_size = forest[0].len();
    let y_size = forest.len();

    let is_on_x_edge = tree_x == 0 || tree_x == x_size - 1;
    let is_on_y_edge = tree_y == 0 || tree_y == y_size - 1;

    if is_on_x_edge || is_on_y_edge {
        return 0;
    }

    let mut top_score = 0;
    let mut bottom_score = 0;
    let mut left_score = 0;
    let mut right_score = 0;

    // Check above
    for y in (0..tree_y).rev() {
        top_score += 1;

        if forest[y][tree_x] >= tree {
            break;
        }
    }

    for y in tree_y + 1..y_size {
        bottom_score += 1;

        if forest[y][tree_x] >= tree {
            break;
        }
    }

    for x in (0..tree_x).rev() {
        left_score += 1;

        if forest[tree_y][x] >= tree {
            break;
        }
    }

    for x in tree_x + 1..x_size {
        right_score += 1;

        if forest[tree_y][x] >= tree {
            break;
        }
    }

    top_score * bottom_score * left_score * right_score
}

#[aoc(day8, part2)]
pub fn part2(input: &Vec<Vec<u32>>) -> i32 {
    let mut scores: Vec<i32> = vec![];

    for y in 0..input.len() {
        for x in 0..input[0].len() {
            scores.push(get_scenic_score(x, y, input));
        }
    }
    scores.sort();

    *scores.last().unwrap()
}
