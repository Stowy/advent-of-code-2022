use std::vec;

use queues::*;

type ElementIndex = usize;

#[derive(Debug)]
pub struct Directory {
    pub name: String,
    pub elements: Vec<ElementIndex>,
    pub parent: ElementIndex,
}

#[derive(Debug)]
pub struct File {
    pub name: String,
    pub size: usize,
}

#[derive(Debug)]
pub enum FSElement {
    Dir(Directory),
    File(File),
}

#[derive(Debug)]
pub struct FileSystem {
    elements: Vec<FSElement>,
    current_dir_index: ElementIndex,
}

impl FileSystem {
    pub fn new() -> FileSystem {
        let root = Directory {
            elements: vec![],
            name: "/".to_string(),
            parent: 0,
        };

        FileSystem {
            elements: vec![FSElement::Dir(root)],
            current_dir_index: 0,
        }
    }

    pub fn get_current_dir_mut(&mut self) -> Option<&mut Directory> {
        match &mut self.elements[self.current_dir_index] {
            FSElement::Dir(dir) => Some(dir),
            _ => None,
        }
    }

    pub fn get_current_dir(&self) -> Option<&Directory> {
        match &self.elements.get(self.current_dir_index)? {
            FSElement::Dir(dir) => Some(dir),
            _ => None,
        }
    }

    pub fn change_dir(&mut self, destination: &str) {
        match destination {
            "/" => {
                self.current_dir_index = 0;
            }
            ".." => {
                let current_dir = self.get_current_dir().unwrap();
                self.current_dir_index = current_dir.parent;
            }
            _ => {
                let (index, _) = self
                    .elements
                    .iter()
                    .enumerate()
                    .find(|(_, elem)| match elem {
                        FSElement::Dir(dir) => dir.name == destination,
                        _ => false,
                    })
                    .unwrap();

                self.current_dir_index = index;
            }
        }
    }

    pub fn add_element(&mut self, element: FSElement) {
        self.elements.push(element);
        let index = self.elements.len() - 1;

        let current_dir = self.get_current_dir_mut().unwrap();
        current_dir.elements.push(index);
    }

    pub fn add_dir(&mut self, name: &str) {
        let dir = FSElement::Dir(Directory {
            name: name.to_string(),
            elements: vec![],
            parent: self.current_dir_index,
        });

        self.add_element(dir);
    }

    pub fn add_file(&mut self, size: usize, name: &str) {
        let file = FSElement::File(File {
            name: name.to_string(),
            size,
        });

        self.add_element(file);
    }

    pub fn list_directories(&self) -> Vec<&ElementIndex> {
        let current_dir = self.get_current_dir().unwrap();
        current_dir
            .elements
            .iter()
            .filter(|index| match self.elements[**index] {
                FSElement::Dir(_) => true,
                _ => false,
            })
            .collect()
    }

    pub fn get_dir_size(&self, dir_index_to_measure: ElementIndex) -> usize {
        let mut dir_queue = queue![dir_index_to_measure];
        let mut sum_size = 0usize;

        while let Ok(dir_index) = dir_queue.remove() {
            let element = &self.elements[dir_index];
            let dir = match element {
                FSElement::Dir(dir) => dir,
                _ => panic!("wtf stowy ?"),
            };

            for sub_elem_index in &dir.elements {
                let element = &self.elements[*sub_elem_index];
                match element {
                    FSElement::Dir(_) => _ = dir_queue.add(*sub_elem_index),
                    FSElement::File(file) => sum_size += file.size,
                }
            }
        }

        sum_size
    }

    pub fn get_all_dirs_size(&self) -> Vec<usize> {
        let mut dir_queue = queue![0usize];
        let mut sizes: Vec<usize> = vec![];

        while let Ok(dir_index) = dir_queue.remove() {
            let element = &self.elements[dir_index];
            let dir = match element {
                FSElement::Dir(dir) => dir,
                _ => panic!("wtf stowy ?"),
            };

            sizes.push(self.get_dir_size(dir_index));

            for sub_elem_index in &dir.elements {
                let element = &self.elements[*sub_elem_index];
                match element {
                    FSElement::Dir(_) => _ = dir_queue.add(*sub_elem_index),
                    _ => {}
                }
            }
        }

        sizes
    }

    pub fn get_small_size_sum(&self) -> usize {
        let mut dir_queue = queue![0usize];
        let mut size_sum: usize = 0;

        while let Ok(dir_index) = dir_queue.remove() {
            let element = &self.elements[dir_index];
            let dir = match element {
                FSElement::Dir(dir) => dir,
                _ => panic!("wtf stowy ?"),
            };

            let size = self.get_dir_size(dir_index);
            if size <= 100_000 {
                size_sum += size;
            }

            for sub_elem_index in &dir.elements {
                let element = &self.elements[*sub_elem_index];
                match element {
                    FSElement::Dir(_) => _ = dir_queue.add(*sub_elem_index),
                    _ => {}
                }
            }
        }

        size_sum
    }
}

#[aoc(day7, part1, Structs)]
pub fn part1(input: &str) -> usize {
    let mut fs = FileSystem::new();

    // Load filesystem
    for line in input.lines() {
        let splitted: Vec<&str> = line.split_whitespace().collect();
        match splitted[0] {
            "$" => match splitted[1] {
                "cd" => {
                    fs.change_dir(splitted[2]);
                }
                "ls" => {}
                _ => {
                    panic!("Bad command :{}", splitted[1]);
                }
            },
            "dir" => fs.add_dir(splitted[1]),
            _ => {
                let size: usize = splitted[0].parse().unwrap();
                fs.add_file(size, splitted[1]);
            }
        }
    }

    fs.get_small_size_sum()
    // let sizes = fs.get_all_dirs_size();

    // dbg!(sizes).iter().filter(|size| **size < 100_000).sum()
}
