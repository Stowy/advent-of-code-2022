use glam::IVec2;

pub fn manhattan_distance(left: IVec2, right: IVec2) -> u32 {
    ((left.x - right.x).abs() + (left.y - right.y).abs()) as u32
}

pub fn get_num_from_part(part: &str, num: i128) -> IVec2 {
    let mut iter = part.split(&[' ', '=', ',']).filter(|&x| !x.is_empty());
    for _ in 0..num {
        iter.next();
    }
    let x: i32 = iter.next().unwrap().parse().unwrap();
    iter.next();
    let y: i32 = iter.next().unwrap().parse().unwrap();

    IVec2::new(x, y)
}

#[aoc(day15, part1)]
pub fn part1(input: &str) -> u32 {
    let mut vec = Vec::new();

    let mut smallest_x = i32::MAX;
    let mut biggest_x = i32::MIN;
    let mut max_dist = u32::MIN;

    for line in input.lines() {
        let (left, right) = line.split_once(':').unwrap();
        let left = get_num_from_part(left, 3);
        let right = get_num_from_part(right, 5);

        if left.x < smallest_x {
            smallest_x = left.x;
        } else if left.x > biggest_x {
            biggest_x = left.x;
        }

        if right.x < smallest_x {
            smallest_x = right.x;
        } else if right.x > biggest_x {
            biggest_x = right.x;
        }

        let dist = manhattan_distance(left, right);
        if dist > max_dist {
            max_dist = dist;
        }

        vec.push((left, dist));
    }

    smallest_x -= max_dist as i32;
    biggest_x += max_dist as i32;

    const Y: i32 = 2_000_000;
    // const Y: i32 = 10;
    let mut count = 0u32;
    for x in smallest_x..=biggest_x {
        let pos = IVec2::new(x, Y);

        for (sensor_pos, dist_to_beacon) in vec.iter() {
            let dist = manhattan_distance(*sensor_pos, pos);
            if dist <= *dist_to_beacon {
                count += 1;
                break;
            }
        }
    }

    if count == 0 {
        return 0;
    }

    count - 1
}

pub fn check_is_outside(pos: IVec2, sensors: &Vec<(IVec2, u32)>) -> bool {
    for (sensor, dist) in sensors {
        let my_dist = manhattan_distance(*sensor, pos);
        if my_dist <= *dist {
            return false;
        }
    }

    true
}

#[aoc(day15, part2)]
pub fn part2(input: &str) -> i128 {
    let mut sensors = Vec::new();

    for line in input.lines() {
        let (left, right) = line.split_once(':').unwrap();
        let left = get_num_from_part(left, 3);

        let right = get_num_from_part(right, 5);

        let dist = manhattan_distance(left, right);

        sensors.push((left, dist));
    }

    const MAX_POS: i32 = 4000000;
    // const MAX_POS: i32 = 20;

    for (sensor_pos, dist) in sensors.iter() {
        let dist_outside = (dist + 1) as i32;
        let start = -dist_outside;
        for x in start..=dist_outside {
            let down_y = (dist_outside - x.abs()).abs();
            let down = IVec2::new(sensor_pos.x + x, sensor_pos.y + down_y);

            let up_y = -down_y;
            let up = IVec2::new(sensor_pos.x + x, sensor_pos.y + up_y);

            let down_result = check_is_outside(down, &sensors);
            let up_result = check_is_outside(up, &sensors);

            if down_result {
                if down.x >= 0 && down.x <= MAX_POS && down.y >= 0 && down.y <= MAX_POS {
                    return (down.x as i128) * 4_000_000 + (down.y as i128);
                }
            }

            if up_result {
                if up.x >= 0 && up.x <= MAX_POS && up.y >= 0 && up.y <= MAX_POS {
                    return (up.x as i128) * 4_000_000 + (up.y as i128);
                }
            }
        }
    }

    800813
}
