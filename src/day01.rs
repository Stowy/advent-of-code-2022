use std::vec;

#[aoc_generator(day1)]
pub fn input_generator(input: &str) -> Vec<Vec<i32>> {
    let mut elfs = vec![];
    let mut one_elf = vec![];
    for line in input.lines() {
        match line {
            "" => {
                elfs.push(one_elf.clone());
                one_elf.clear();
            }
            _ => {
                let parse = line.parse::<i32>();
                if parse.is_ok() {
                    one_elf.push(parse.unwrap());
                }
            }
        }
    }

    elfs
}

#[aoc(day1, part1)]
pub fn part1(input: &Vec<Vec<i32>>) -> i32 {
    let mut sums: Vec<i32> = vec![];
    input.iter().for_each(|elf| sums.push(elf.iter().sum()));
    *sums
        .iter()
        .max_by(|x, y| x.abs().partial_cmp(&y.abs()).unwrap())
        .unwrap()
}

#[aoc(day1, part2)]
pub fn part2(input: &Vec<Vec<i32>>) -> i32 {
    let mut sums: Vec<i32> = vec![];
    input.iter().for_each(|elf| sums.push(elf.iter().sum()));
    sums.sort();
    let index = sums.len() - 3;
    sums[index..].iter().sum::<i32>()
}
