use std::{
    cmp::Ordering,
    fmt::{self, Display},
};

#[derive(Debug, Clone)]
pub enum Element {
    List(Vec<Element>),
    Num(u32),
}

impl Display for Element {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Element::List(list) => {
                let mut text = String::from("[");
                for i in 0..list.len() {
                    if i == list.len() - 1 {
                        text += format!("{}", list[i]).as_str();
                    } else {
                        text += format!("{},", list[i]).as_str();
                    }
                }
                text.push(']');

                write!(f, "{}", text)
            }
            Element::Num(num) => write!(f, "{}", num),
        }
    }
}

impl Ord for Element {
    fn cmp(&self, other: &Self) -> Ordering {
        let result = self.compare(other);

        match result {
            Some(true) => Ordering::Less,
            Some(false) => Ordering::Greater,
            _ => Ordering::Equal,
        }
    }
}

impl PartialOrd for Element {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Element {
    fn eq(&self, other: &Self) -> bool {
        match self {
            Element::List(list) => match other {
                Element::List(list_other) => {
                    for elem in list.iter() {
                        for elem_other in list_other.iter() {
                            if !elem.eq(elem_other) {
                                return false;
                            }
                        }
                    }

                    true
                }
                Element::Num(_) => false,
            },
            Element::Num(num) => match other {
                Element::List(_) => false,
                Element::Num(num_other) => num.eq(num_other),
            },
        }
    }
}

impl Eq for Element {}

impl Element {
    pub fn compare(&self, right: &Element) -> Option<bool> {
        match (self, right) {
            (Element::Num(left_num), Element::Num(right_num)) => {
                if left_num < right_num {
                    return Some(true);
                } else if right_num < left_num {
                    return Some(false);
                }
            }
            (Element::List(left_list), Element::List(right_list)) => {
                let mut left_iter = left_list.iter();
                let mut right_iter = right_list.iter();

                loop {
                    match (left_iter.next(), right_iter.next()) {
                        (None, Some(_)) => return Some(true),
                        (Some(_), None) => return Some(false),
                        (Some(left), Some(right)) => {
                            if let Some(result) = left.compare(right) {
                                return Some(result);
                            }
                        }
                        (None, None) => {
                            break;
                        }
                    }
                }
            }
            (Element::List(_), Element::Num(right_num)) => {
                let right_list = Element::List(vec![Element::Num(*right_num)]);
                if let Some(result) = self.compare(&right_list) {
                    return Some(result);
                }
            }
            (Element::Num(left_num), Element::List(_)) => {
                let left_list = Element::List(vec![Element::Num(*left_num)]);
                if let Some(result) = left_list.compare(&right) {
                    return Some(result);
                }
            }
        }

        None
    }
}

pub fn parse_line(line: &str, stack: &mut Vec<Element>) -> Option<Element> {
    let mut last_elem = None;
    let chars: Vec<char> = line.chars().collect();
    let mut i = 0;

    while i < chars.len() {
        let char = chars[i];
        match char {
            '[' => stack.push(Element::List(Vec::new())),
            ']' => {
                if let Some(Element::List(mut last)) = stack.pop() {
                    if let Some(elem) = last_elem {
                        last.push(elem);
                        last_elem = None;
                    }
                    match &mut stack.last_mut() {
                        Some(Element::List(_)) => {
                            last_elem = Some(Element::List(last));
                        }
                        None => {
                            return Some(Element::List(last));
                        }
                        _ => {}
                    }
                }
            }
            ',' => {
                if let Element::List(list) = stack.last_mut().unwrap() {
                    if let Some(elem) = last_elem {
                        last_elem = None;
                        list.push(elem);
                    }
                }
            }
            _ => {
                let mut next = i;
                let mut string = String::new();
                while !"[],".contains(*chars.get(next).unwrap_or(&'[')) {
                    string.push(chars[next]);
                    next += 1;
                }
                last_elem = Some(Element::Num(string.parse().unwrap()));
                i += next - i - 1;
            }
        }

        i += 1;
    }

    None
}

#[aoc(day13, part1)]
pub fn part1(input: &str) -> usize {
    let mut stack: Vec<Element> = Vec::new();
    let mut input_iter = input.lines();
    let mut pair_index = 0;

    let mut good_order = Vec::new();
    let mut bad_order = Vec::new();

    while let Some(line_left_str) = input_iter.next() {
        pair_index += 1;
        let line_left = parse_line(line_left_str, &mut stack).unwrap();

        let line_right_str = input_iter.next().unwrap();
        let line_right = parse_line(line_right_str, &mut stack).unwrap();
        input_iter.next();

        match line_left.compare(&line_right) {
            Some(true) => good_order.push(pair_index),
            Some(false) => bad_order.push(pair_index),
            _ => panic!("weird"),
        };
    }

    good_order.iter().sum()
}

#[aoc(day13, part2)]
pub fn part2(input: &str) -> usize {
    let mut stack: Vec<Element> = Vec::new();
    let mut input_iter = input.lines();

    let mut elements = Vec::new();

    while let Some(line_left_str) = input_iter.next() {
        let line_left = parse_line(line_left_str, &mut stack).unwrap();
        elements.push(line_left);

        let line_right_str = input_iter.next().unwrap();
        let line_right = parse_line(line_right_str, &mut stack).unwrap();
        elements.push(line_right);
        input_iter.next();
    }

    let divider_one = Element::List(vec![Element::List(vec![Element::Num(2)])]);
    let divider_two = Element::List(vec![Element::List(vec![Element::Num(6)])]);
    elements.push(divider_one.clone());
    elements.push(divider_two.clone());

    elements.sort();

    // elements.iter().for_each(|elem| println!("{}", elem));

    let mut a = 0;
    let mut b = 0;

    for i in 0..elements.len() {
        if elements[i] == divider_one {
            a = i + 1;
        } else if elements[i] == divider_two {
            b = i + 1;
        }
    }

    a * b
}
