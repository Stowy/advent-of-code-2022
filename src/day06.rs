use itertools::Itertools;

const START_OF_PACKET_LENGTH_PART_1: usize = 4;
const START_OF_PACKET_LENGTH_PART_2: usize = 14;

pub fn add_char(array: &mut [char], char: char) {
    for i in 0..array.len() {
        if let Some(elem) = array.get_mut(i + 1) {
            array[i] = *elem;
        }
    }

    let last = array.last_mut().unwrap();
    *last = char;
}

pub fn are_all_unique(array: &[char], len: usize) -> bool {
    array.into_iter().unique().count() == len
}

#[aoc(day6, part1)]
pub fn part1(input: &str) -> i32 {
    let mut count = 0;
    let mut last_chars = [' '; START_OF_PACKET_LENGTH_PART_1];

    for (index, character) in input.char_indices() {
        count += 1;

        add_char(&mut last_chars, character);

        if index >= START_OF_PACKET_LENGTH_PART_1 {
            if are_all_unique(&last_chars, START_OF_PACKET_LENGTH_PART_1) {
                return count;
            }
        }
    }

    0
}

#[aoc(day6, part2)]
pub fn part2(input: &str) -> i32 {
    let mut count = 0;
    let mut last_chars = [' '; START_OF_PACKET_LENGTH_PART_2];

    for (index, character) in input.char_indices() {
        count += 1;

        add_char(&mut last_chars, character);

        if index >= START_OF_PACKET_LENGTH_PART_2 {
            if are_all_unique(&last_chars, START_OF_PACKET_LENGTH_PART_2) {
                return count;
            }
        }
    }

    0
}
