use std::vec;

#[derive(Debug, Clone)]
pub struct Crane {
    pub stacks: Vec<Vec<char>>,
}

impl Crane {
    pub fn from(stacks: Vec<Vec<char>>) -> Self {
        Self { stacks }
    }

    pub fn displace_part1(&mut self, instruction: &Instruction) {
        for _ in 0..instruction.quantity {
            let value = self.stacks[instruction.from].pop().unwrap();
            self.stacks[instruction.to].push(value);
        }
    }

    pub fn displace_part2(&mut self, instruction: &Instruction) {
        let mut moved: Vec<char> = vec![];
        for _ in 0..instruction.quantity {
            let value = self.stacks[instruction.from].pop().unwrap();
            moved.push(value);
        }

        for char in moved.iter().rev() {
            self.stacks[instruction.to].push(*char);
        }
    }

    pub fn top_string(&self) -> String {
        let mut string = String::new();
        for stack in self.stacks.iter() {
            string.push(*stack.last().unwrap());
        }

        string
    }
}

#[derive(Debug)]
pub struct Instruction {
    quantity: usize,
    from: usize,
    to: usize,
}

impl Instruction {
    pub fn from(string: &str) -> Self {
        let mut splitted = string.split(' ');
        splitted.next();
        let quantity: usize = splitted.next().unwrap().parse().unwrap();
        splitted.next();
        let from: usize = splitted.next().unwrap().parse::<usize>().unwrap() - 1;
        splitted.next();
        let to: usize = splitted.next().unwrap().parse::<usize>().unwrap() - 1;

        Self { quantity, from, to }
    }
}

pub fn get_crane_data(input: &str) -> Vec<Vec<char>> {
    let mut crane_data: Vec<Vec<char>> = vec![];
    let lines: Vec<Vec<char>> = input
        .lines()
        .filter(|line| line.contains('[') || line.starts_with(" 1 "))
        .map(|line| line.chars().collect())
        .collect();

    let last_line = lines.last().unwrap();
    let stacks_num: usize = last_line[last_line.len() - 2]
        .to_digit(10)
        .unwrap()
        .try_into()
        .unwrap();

    crane_data.resize(stacks_num, vec![]);

    let max_stack_len = lines.len() - 1;

    for x in 0..stacks_num {
        for y in 0..max_stack_len {
            let char_x = x * 4 + 1;
            let char = lines[y][char_x];
            if char == ' ' {
                continue;
            }
            crane_data[x].push(char);
        }
        crane_data[x].reverse();
    }

    crane_data
}

#[aoc_generator(day5)]
pub fn generator(input: &str) -> (Crane, Vec<Instruction>) {
    let crane_data: Vec<Vec<char>> = get_crane_data(input);

    let instructions: Vec<Instruction> = input
        .lines()
        .filter(|line| line.starts_with("move "))
        .map(|line| Instruction::from(line))
        .collect();

    (Crane::from(crane_data), instructions)
}

#[aoc(day5, part1)]
pub fn part1((input_crane, instructions): &(Crane, Vec<Instruction>)) -> String {
    let mut crane: Crane = input_crane.clone();
    for instruction in instructions {
        crane.displace_part1(instruction);
    }

    crane.top_string()
}

#[aoc(day5, part2)]
pub fn part2((input_crane, instructions): &(Crane, Vec<Instruction>)) -> String {
    let mut crane: Crane = input_crane.clone();
    for instruction in instructions {
        crane.displace_part2(instruction);
    }

    crane.top_string()
}
