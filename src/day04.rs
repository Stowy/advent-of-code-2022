#[derive(Debug)]
pub struct Section {
    min: i32,
    max: i32,
}

impl Section {
    pub fn contains(&self, other: &Self) -> bool {
        self.min <= other.min && self.max >= other.max
    }

    pub fn overlaps(&self, other: &Self) -> bool {
        !(self.min > other.max || other.min > self.max)
    }

    pub fn from(string: &str) -> Self {
        let splitted = string.split_once('-').unwrap();
        let min: i32 = splitted.0.parse().unwrap();
        let max: i32 = splitted.1.parse().unwrap();
        Section { min, max }
    }
}

pub fn convert_section(input: &(&str, &str)) -> (Section, Section) {
    (Section::from(input.0), Section::from(input.1))
}

#[aoc(day4, part1, NoGenerator)]
pub fn part1_no_generator(input: &str) -> usize {
    input
        .lines()
        .map(|line| convert_section(&line.split_once(',').unwrap()))
        .filter(|(a, b)| a.contains(b) || b.contains(a))
        .count()
}

#[aoc_generator(day4, part1, Generator)]
pub fn generator_part1(input: &str) -> Vec<(Section, Section)> {
    input
        .lines()
        .map(|line| convert_section(&line.split_once(',').unwrap()))
        .collect()
}

#[aoc(day4, part1, Generator)]
pub fn part1(input: &Vec<(Section, Section)>) -> usize {
    input
        .iter()
        .filter(|(a, b)| a.contains(b) || b.contains(a))
        .count()
}

#[aoc(day4, part2)]
pub fn part2_no_generator(input: &str) -> usize {
    input
        .lines()
        .map(|line| convert_section(&line.split_once(',').unwrap()))
        .filter(|(a, b)| a.overlaps(b) || b.overlaps(a))
        .count()
}
