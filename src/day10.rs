pub enum Instruction {
    Noop,
    Addx(i32),
}

pub struct CPU {
    pub register_x: i32,
    pub cycle_count: i32,
    instructions: Vec<Instruction>,
    addx_counter: i32,
    pub cycles_to_return: Vec<i32>,
    pub screen: String,
    crt_pos: i32,
}

impl CPU {
    pub fn new() -> CPU {
        CPU {
            register_x: 1,
            cycle_count: 1,
            instructions: vec![],
            addx_counter: 0,
            cycles_to_return: vec![20, 60, 100, 140, 180, 220],
            screen: String::from("\n"),
            crt_pos: 0,
        }
    }

    pub fn instructions_len(&self) -> usize {
        self.instructions.len()
    }

    pub fn add_instruction(&mut self, instruction: Instruction) {
        self.instructions.push(instruction);
    }

    pub fn draw_screen(&mut self) {
        let diff = (self.crt_pos - self.register_x).abs();
        self.screen.push(if diff > 1 { ' ' } else { '#' });
        self.crt_pos += 1;
    }

    pub fn tick(&mut self) -> Option<i32> {
        self.draw_screen();
        self.cycle_count += 1;

        match self.instructions[0] {
            Instruction::Noop => {
                self.instructions.remove(0);
            }
            Instruction::Addx(val) => {
                if self.addx_counter == 0 {
                    self.addx_counter += 1;
                } else {
                    self.addx_counter = 0;
                    self.instructions.remove(0);
                    self.register_x += val;
                }
            }
        }

        if self.cycles_to_return.contains(&self.cycle_count) {
            self.screen.push('\n');
            self.crt_pos = 0;
            Some(self.cycle_count * self.register_x)
        } else {
            None
        }
    }
}

#[aoc(day10, part1)]
pub fn part1(input: &str) -> i32 {
    let mut cpu = CPU::new();

    // fill cpu instructions
    for line in input.lines() {
        if line == "noop" {
            cpu.add_instruction(Instruction::Noop);
        } else if line.starts_with("addx") {
            let mut splitted = line.split_whitespace();
            splitted.next();

            let num: i32 = splitted.next().unwrap().parse().unwrap();
            cpu.add_instruction(Instruction::Addx(num));
        }
    }

    let mut sum = 0;

    while cpu.instructions_len() > 0 {
        let result = cpu.tick();

        match result {
            Some(val) => {
                sum += val;
                dbg!(val);
            }
            None => {}
        }
    }

    sum
}

#[aoc(day10, part2)]
pub fn part2(input: &str) -> String {
    let mut cpu = CPU::new();
    cpu.cycles_to_return = vec![40, 80, 120, 160, 200, 240];
    cpu.cycle_count = 0;

    // fill cpu instructions
    for line in input.lines() {
        if line == "noop" {
            cpu.add_instruction(Instruction::Noop);
        } else if line.starts_with("addx") {
            let mut splitted = line.split_whitespace();
            splitted.next();

            let num: i32 = splitted.next().unwrap().parse().unwrap();
            cpu.add_instruction(Instruction::Addx(num));
        }
    }

    while cpu.instructions_len() > 0 {
        cpu.tick();
    }

    cpu.screen
}
