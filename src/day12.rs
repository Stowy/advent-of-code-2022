// If you read this, i am sorry...
use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::usize;

struct Grid {
    nodes: Vec<Vec<i32>>,
}

#[derive(Copy, Clone, Eq, PartialEq)]
struct State {
    node: (usize, usize),
    cost: usize,
}

// Manually implement Ord so we get a min-heap instead of a max-heap
impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        other.cost.cmp(&self.cost)
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Grid {
    fn new(grid_vec: Vec<Vec<i32>>) -> Self {
        Grid { nodes: grid_vec }
    }

    fn find_path(
        &self,
        start: (usize, usize),
        end: (usize, usize),
    ) -> Option<(Vec<(usize, usize)>, usize)> {
        let mut dist = vec![vec![(usize::MAX, None); self.nodes[0].len()]; self.nodes.len()];

        let mut heap = BinaryHeap::new();
        dist[start.1][start.0] = (0, None);
        heap.push(State {
            node: start,
            cost: 0,
        });

        while let Some(State { node, cost }) = heap.pop() {
            if node == end {
                let mut path = Vec::with_capacity(dist.len() / 2);
                let mut current_dist = dist[end.1][end.0];
                path.push(end);
                while let Some(prev) = current_dist.1 {
                    path.push(prev);
                    current_dist = dist[prev.1][prev.0];
                }
                path.reverse();
                return Some((path, cost));
            }

            if cost > dist[node.1][node.0].0 {
                continue;
            }

            let neighboors = get_neighboors(&self.nodes, node.0, node.1);

            add_neighboor(neighboors.up, cost, &mut dist, node, &mut heap);
            add_neighboor(neighboors.down, cost, &mut dist, node, &mut heap);
            add_neighboor(neighboors.left, cost, &mut dist, node, &mut heap);
            add_neighboor(neighboors.right, cost, &mut dist, node, &mut heap);
        }
        None
    }
}

fn add_neighboor(
    dir: Option<((usize, usize), usize)>,
    cost: usize,
    dist: &mut Vec<Vec<(usize, Option<(usize, usize)>)>>,
    node: (usize, usize),
    heap: &mut BinaryHeap<State>,
) {
    match dir {
        Some(dir) => {
            let next = State {
                node: dir.0,
                cost: cost + dir.1,
            };
            if next.cost < dist[next.node.1][next.node.0].0 {
                dist[next.node.1][next.node.0] = (next.cost, Some(node));
                heap.push(next);
            }
        }
        None => {}
    }
}

pub fn get_height(mut char: char) -> i32 {
    if char == 'S' {
        char = 'a';
    } else if char == 'E' {
        char = 'z';
    }

    char as i32
}

#[aoc_generator(day12)]
pub fn generator(input: &str) -> (Vec<Vec<i32>>, (usize, usize), (usize, usize)) {
    // let lines: Vec<&str> = input.lines().collect();
    let mut start = (0, 0);
    let mut end = (0, 0);
    let heights: Vec<Vec<i32>> = input
        .lines()
        .enumerate()
        .map(|(y, line)| {
            line.chars()
                .enumerate()
                .map(|(x, char)| {
                    if char == 'S' {
                        start = (x, y);
                    } else if char == 'E' {
                        end = (x, y);
                    }
                    get_height(char)
                })
                .collect()
        })
        .collect();

    (heights, start, end)
}

pub struct Neighboors {
    pub up: Option<((usize, usize), usize)>,
    pub down: Option<((usize, usize), usize)>,
    pub left: Option<((usize, usize), usize)>,
    pub right: Option<((usize, usize), usize)>,
}

pub fn get_neighboors(heights: &Vec<Vec<i32>>, x: usize, y: usize) -> Neighboors {
    let x = x as i32;
    let y = y as i32;

    let up = (x, y - 1);
    let down = (x, y + 1);
    let left = (x - 1, y);
    let right = (x + 1, y);

    Neighboors {
        up: check_height(up, heights, x, y),
        down: check_height(down, heights, x, y),
        left: check_height(left, heights, x, y),
        right: check_height(right, heights, x, y),
    }
}

pub fn is_oob(heights: &Vec<Vec<i32>>, (x, y): (i32, i32)) -> bool {
    let is_oob_x = x < 0 || x >= heights[0].len() as i32;
    let is_oob_y = y < 0 || y >= heights.len() as i32;
    is_oob_x || is_oob_y
}

fn check_height(
    dir: (i32, i32),
    heights: &Vec<Vec<i32>>,
    x: i32,
    y: i32,
) -> Option<((usize, usize), usize)> {
    if !is_oob(heights, dir) {
        let dir = (dir.0 as usize, dir.1 as usize);
        let elem = heights[dir.1][dir.0];
        if elem > heights[y as usize][x as usize] + 1 {
            // Some((dir, usize::MAX))
            None
        } else {
            Some((dir, 1))
        }
    } else {
        None
    }
}

#[aoc(day12, part1)]
pub fn part1((heights, start, end): &(Vec<Vec<i32>>, (usize, usize), (usize, usize))) -> usize {
    let grid = Grid::new(heights.clone());

    println!("Finding cost");
    let (_, cost) = grid.find_path(*start, *end).unwrap();
    cost
}

#[aoc(day12, part2)]
pub fn part2((heights, _, end): &(Vec<Vec<i32>>, (usize, usize), (usize, usize))) -> usize {
    let grid = Grid::new(heights.clone());
    let mut costs = Vec::new();
    let a_height = get_height('a');

    for y in 0..heights.len() {
        for x in 0..heights[0].len() {
            if heights[y][x] != a_height {
                continue;
            }

            match grid.find_path((x, y), *end) {
                Some((_, cost)) => costs.push(cost),
                None => {}
            }
        }
    }

    costs.sort();

    costs[0]
}
