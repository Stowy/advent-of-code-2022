use std::collections::HashMap;

#[derive(Debug)]
pub struct Valve {
    name: String,
    flow_rate: i32,
    leads_to: Vec<usize>,
}

#[aoc_generator(day16)]
pub fn generator(input: &str) -> Vec<Valve> {
    let mut valves = Vec::new();
    let mut map = HashMap::new();

    for line in input.lines() {
        let mut splitted = line.split(&[' ', '=', ';']);
        splitted.next();
        let name = splitted.next().unwrap().to_string();
        splitted.next();
        splitted.next();
        splitted.next();
        let flow_rate: i32 = splitted.next().unwrap().parse().unwrap();
        valves.push(Valve {
            name: name.clone(),
            flow_rate,
            leads_to: vec![],
        });
        map.insert(name, valves.len());
    }

    for (i, line) in input.lines().enumerate() {
        let leads_str = line.split_once(';').unwrap().1;
        let mut splitted = leads_str.splitn(6, ' ');
        splitted.next();
        splitted.next();
        splitted.next();
        splitted.next();
        splitted.next();
        let valves_str = splitted.next().unwrap().split(", ");

        for valve_str in valves_str {
            let j = map[valve_str];
            valves[i].leads_to.push(j);
        }
    }

    valves
}

#[aoc(day16, part1)]
pub fn part1(input: &Vec<Valve>) -> i32 {
    dbg!(input);

    0
}
