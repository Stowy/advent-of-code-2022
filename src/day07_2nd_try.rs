// i lost... https://gist.github.com/dev-ardi/05b5ae69098eb4e566e197269710dd8c
#[aoc(day7, part1, Stack)]
pub fn part1(input: &str) -> usize {
    let mut sum = 0usize;
    let mut stack: Vec<usize> = vec![];

    for line in input.lines() {
        if line == "$ cd .." {
            let val = stack.pop().unwrap();
            if val < 100_000 {
                sum += val;
            }
        } else if line.starts_with("$ cd") {
            stack.push(0);
        } else if line.starts_with(|c: char| c.is_numeric()) {
            let val: usize = line.split_whitespace().next().unwrap().parse().unwrap();
            stack.iter_mut().for_each(|num| *num += val);
        }
    }

    sum
}

const SPACE_AVAILABLE: usize = 70_000_000;
// I computed this beforehand
const SPACE_TAKEN: usize = 48_044_502;
const SPACE_FREE: usize = SPACE_AVAILABLE - SPACE_TAKEN;
const MIN_SPACE: usize = 30_000_000;
const SPACE_TO_FREE: usize = MIN_SPACE - SPACE_FREE;

#[aoc(day7, part2)]
pub fn part2(input: &str) -> usize {
    let mut stack: Vec<usize> = vec![];
    let mut sizes: Vec<usize> = vec![];

    for line in input.lines() {
        if line == "$ cd .." {
            let val = stack.pop().unwrap();
            sizes.push(val);
        } else if line.starts_with("$ cd") {
            stack.push(0);
        } else if line.starts_with(|c: char| c.is_numeric()) {
            let val: usize = line.split_whitespace().next().unwrap().parse().unwrap();
            stack.iter_mut().for_each(|num| *num += val);
        }
    }

    sizes.sort();
    let filtered: Vec<&usize> = sizes
        .iter()
        .filter(|size| **size >= SPACE_TO_FREE)
        .collect();
    *filtered[0]
}
