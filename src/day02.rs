#[derive(Debug)]
pub enum BattleState {
    Win,
    Lose,
    Draw,
}

impl BattleState {
    pub fn score(&self) -> i32 {
        match self {
            BattleState::Win => 6,
            BattleState::Lose => 0,
            BattleState::Draw => 3,
        }
    }

    pub fn build(char: char) -> Result<Self, ()> {
        match char {
            'X' => Ok(Self::Lose),
            'Y' => Ok(Self::Draw),
            'Z' => Ok(Self::Win),
            _ => Err(()),
        }
    }
}

#[derive(Debug)]
pub enum Shape {
    Rock,
    Paper,
    Scissor,
}

impl Shape {
    pub fn build(char: char) -> Result<Self, ()> {
        match char {
            'A' | 'X' => Ok(Self::Rock),
            'B' | 'Y' => Ok(Self::Paper),
            'C' | 'Z' => Ok(Self::Scissor),
            _ => Err(()),
        }
    }

    pub fn score(&self) -> i32 {
        match self {
            Shape::Rock => 1,
            Shape::Paper => 2,
            Shape::Scissor => 3,
        }
    }

    pub fn battle(&self, other: &Self) -> BattleState {
        match self {
            Shape::Rock => match other {
                Shape::Rock => BattleState::Draw,
                Shape::Paper => BattleState::Lose,
                Shape::Scissor => BattleState::Win,
            },
            Shape::Paper => match other {
                Shape::Rock => BattleState::Win,
                Shape::Paper => BattleState::Draw,
                Shape::Scissor => BattleState::Lose,
            },
            Shape::Scissor => match other {
                Shape::Rock => BattleState::Lose,
                Shape::Paper => BattleState::Win,
                Shape::Scissor => BattleState::Draw,
            },
        }
    }

    pub fn from_result(&self, state: &BattleState) -> Self {
        match self {
            Shape::Rock => match state {
                BattleState::Win => Self::Paper,
                BattleState::Lose => Self::Scissor,
                BattleState::Draw => Self::Rock,
            },
            Shape::Paper => match state {
                BattleState::Win => Self::Scissor,
                BattleState::Lose => Self::Rock,
                BattleState::Draw => Self::Paper,
            },
            Shape::Scissor => match state {
                BattleState::Win => Self::Rock,
                BattleState::Lose => Self::Paper,
                BattleState::Draw => Self::Scissor,
            },
        }
    }
}

#[aoc_generator(day2, part1)]
pub fn input_generator_part1(input: &str) -> Vec<(Shape, Shape)> {
    let mut moves = vec![];
    for line in input.lines() {
        let mut chars = line.chars();
        let first = chars.next().unwrap();
        chars.next();
        let second = chars.next().unwrap();

        let first_move = Shape::build(first).unwrap();
        let second_move = Shape::build(second).unwrap();

        moves.push((first_move, second_move));
    }

    moves
}

#[aoc(day2, part1)]
pub fn part1(input: &Vec<(Shape, Shape)>) -> i32 {
    let mut sum = 0;
    for round in input {
        let opponent = &round.0;
        let me = &round.1;
        sum += me.score();
        let result = me.battle(opponent);
        sum += result.score();
    }

    sum
}

#[aoc_generator(day2, part2)]
pub fn input_generator_part2(input: &str) -> Vec<(Shape, BattleState)> {
    let mut moves = vec![];
    for line in input.lines() {
        let mut chars = line.chars();
        let first = chars.next().unwrap();
        chars.next();
        let second = chars.next().unwrap();

        let shape = Shape::build(first).unwrap();
        let output = BattleState::build(second).unwrap();

        moves.push((shape, output));
    }

    moves
}

#[aoc(day2, part2)]
pub fn part2(input: &Vec<(Shape, BattleState)>) -> i32 {
    let mut sum = 0;
    for round in input {
        let opponent = &round.0;
        let battle_state = &round.1;
        sum += battle_state.score();
        let move_to_do = opponent.from_result(battle_state);
        sum += move_to_do.score();
    }

    sum
}
