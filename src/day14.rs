use glam::UVec2;
use grid::Grid;

#[derive(Default, PartialEq, Eq)]
pub enum BlockType {
    #[default]
    Air,
    Stone,
    Sand,
}

#[derive(Debug)]
pub enum ProblemType {
    NotAir,
    OutOfBounds,
}

const GRID_SIZE: usize = 1000;

pub fn check_air(grid: &Grid<BlockType>, pos: UVec2) -> Result<UVec2, ProblemType> {
    let x = pos.x as usize;
    let y = pos.y as usize;
    let block = grid.get(x, y);

    if let Some(block) = block {
        match block {
            BlockType::Air => Ok(pos),
            _ => Err(ProblemType::NotAir),
        }
    } else {
        Err(ProblemType::OutOfBounds)
    }
}

pub fn get_next_sand_pos(grid: &Grid<BlockType>, start_pos: UVec2) -> Result<UVec2, ProblemType> {
    let bottom = UVec2::new(start_pos.x, start_pos.y + 1);
    let bottom_check = check_air(grid, bottom);

    if bottom_check.is_ok() {
        return Ok(bottom);
    }

    if start_pos.x == 0 {
        return Err(ProblemType::NotAir);
    }

    let left = UVec2::new(start_pos.x - 1, start_pos.y + 1);
    let right = UVec2::new(start_pos.x + 1, start_pos.y + 1);
    let left_check = check_air(grid, left);

    if left_check.is_ok() {
        return Ok(left);
    }

    let right_check = check_air(grid, right);

    if right_check.is_ok() {
        return Ok(right);
    } else {
        return Err(right_check.err().unwrap());
    }
}

pub fn fall_once(grid: &mut Grid<BlockType>, sand_pos: UVec2) -> Result<UVec2, ProblemType> {
    let next_pos = get_next_sand_pos(grid, sand_pos)?;
    grid[sand_pos.x as usize][sand_pos.y as usize] = BlockType::Air;
    grid[next_pos.x as usize][next_pos.y as usize] = BlockType::Sand;

    Ok(next_pos)
}

pub fn fall_sand(grid: &mut Grid<BlockType>) -> bool {
    let start_point = UVec2::new(500, 0);
    let start_x = start_point.x as usize;
    let start_y = start_point.y as usize;

    if grid[start_x][start_y] != BlockType::Air {
        return false;
    }

    grid[start_x][start_y] = BlockType::Sand;

    let mut last_pos = start_point;

    loop {
        let next_pos = fall_once(grid, last_pos);

        if next_pos.is_ok() {
            last_pos = next_pos.unwrap();
        } else {
            let err = next_pos.err().unwrap();
            match err {
                ProblemType::NotAir => return true,
                ProblemType::OutOfBounds => return false,
            }
        }
    }
}

pub fn fill(grid: &mut Grid<BlockType>, start: UVec2, end: UVec2) {
    let mut start_x = start.x as usize;
    let mut end_x = end.x as usize;

    if end_x < start_x {
        start_x = end.x as usize;
        end_x = start.x as usize;
    }

    for x in start_x..=end_x {
        let mut start_y = start.y as usize;
        let mut end_y = end.y as usize;

        if end_y < start_y {
            start_y = end.y as usize;
            end_y = start.y as usize;
        }

        for y in start_y..=end_y {
            let x = x as usize;
            let y = y as usize;
            grid[x][y] = BlockType::Stone;
        }
    }
}

#[aoc(day14, part1)]
pub fn part1(input: &str) -> i32 {
    let mut grid: Grid<BlockType> = Grid::new(GRID_SIZE, GRID_SIZE);

    let parsed: Vec<Vec<UVec2>> = input
        .lines()
        .map(|line| {
            line.split(" -> ")
                .map(|pos_str| {
                    let splitted = pos_str.split_once(',').unwrap();
                    UVec2::new(splitted.0.parse().unwrap(), splitted.1.parse().unwrap())
                })
                .collect()
        })
        .collect();

    for line in parsed {
        for i in 1..line.len() {
            let last = line[i - 1];
            let current = line[i];
            fill(&mut grid, last, current);
        }
    }

    let mut count = 0;

    while fall_sand(&mut grid) {
        count += 1;
    }

    count
}

#[aoc(day14, part2)]
pub fn part2(input: &str) -> i32 {
    let mut grid: Grid<BlockType> = Grid::new(GRID_SIZE, GRID_SIZE);
    let mut biggest_y = 0u32;

    let parsed: Vec<Vec<UVec2>> = input
        .lines()
        .map(|line| {
            line.split(" -> ")
                .map(|pos_str| {
                    let splitted = pos_str.split_once(',').unwrap();
                    let y: u32 = splitted.1.parse().unwrap();
                    if biggest_y < y {
                        biggest_y = y;
                    }
                    UVec2::new(splitted.0.parse().unwrap(), y)
                })
                .collect()
        })
        .collect();

    for line in parsed {
        for i in 1..line.len() {
            let last = line[i - 1];
            let current = line[i];
            fill(&mut grid, last, current);
        }
    }

    let ground_y = 2 + biggest_y;
    fill(
        &mut grid,
        UVec2::new(0, ground_y),
        UVec2::new((GRID_SIZE - 1) as u32, ground_y),
    );

    let mut count = 0;

    while fall_sand(&mut grid) {
        count += 1;
    }

    count
}
