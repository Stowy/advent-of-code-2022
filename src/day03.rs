pub fn get_priority(char: char) -> i32 {
    let as_int = char as i32;
    if char.is_lowercase() {
        as_int - 96
    } else {
        as_int - 38
    }
}

pub fn find_common_char(first: &str, second: &str) -> Option<char> {
    for char in first.chars() {
        if second.contains(char) {
            return Some(char);
        }
    }

    None
}

#[aoc(day3, part1)]
pub fn part1(input: &str) -> i32 {
    let mut sum = 0;
    for line in input.lines() {
        let half_len = line.len() / 2;
        let first_part = &line[..half_len];
        let second_part = &line[half_len..];

        let char = find_common_char(first_part, second_part).unwrap_or(0 as char);
        sum += get_priority(char);
    }
    sum
}

#[aoc_generator(day3, part2)]
pub fn input_generator_part2(input: &str) -> Vec<[String; 3]> {
    let mut groups: Vec<[String; 3]> = vec![];
    let lines: Vec<&str> = input.lines().collect();
    let mut iterator = lines.iter();

    let divided = lines.len() / 3;
    for _ in 0..divided {
        let first = iterator.next().unwrap();
        let second = iterator.next().unwrap();
        let third = iterator.next().unwrap();

        groups.push([first.to_string(), second.to_string(), third.to_string()]);
    }

    groups
}

pub fn find_common_char_in_array(array: &[String; 3]) -> Option<char> {
    for char in array[0].chars() {
        if array[1].contains(char) && array[2].contains(char) {
            return Some(char);
        }
    }

    None
}

#[aoc(day3, part2)]
pub fn part2(input: &Vec<[String; 3]>) -> i32 {
    let mut sum = 0;
    for group in input {
        let char = find_common_char_in_array(group).unwrap_or(0 as char);
        sum += get_priority(char);
    }

    sum
}
