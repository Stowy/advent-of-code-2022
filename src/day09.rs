use glam::IVec2;
use std::collections::HashSet;

pub struct SmallRope {
    head_pos: IVec2,
    tail_pos: IVec2,
}

impl SmallRope {
    pub fn new() -> SmallRope {
        SmallRope {
            head_pos: IVec2::new(0, 0),
            tail_pos: IVec2::new(0, 0),
        }
    }

    pub fn update_tail(&mut self) {
        let diff = self.head_pos - self.tail_pos;

        match (diff.x.abs() > 1, diff.y.abs() > 1) {
            (true, true) => self.tail_pos += IVec2::new(diff.x.signum(), diff.y.signum()),
            (true, false) => {
                self.tail_pos = IVec2::new(self.tail_pos.x + diff.x.signum(), self.head_pos.y)
            }
            (false, true) => {
                self.tail_pos = IVec2::new(self.head_pos.x, self.tail_pos.y + diff.y.signum())
            }
            _ => {}
        };
    }

    pub fn move_up(&mut self) {
        self.head_pos += IVec2::Y;
        self.update_tail();
    }

    pub fn move_down(&mut self) {
        self.head_pos -= IVec2::Y;
        self.update_tail();
    }

    pub fn move_right(&mut self) {
        self.head_pos += IVec2::X;
        self.update_tail();
    }

    pub fn move_left(&mut self) {
        self.head_pos -= IVec2::X;
        self.update_tail();
    }
}

#[aoc(day9, part1)]
pub fn part1(input: &str) -> usize {
    let mut rope = SmallRope::new();
    let mut unique_pos = HashSet::new();
    unique_pos.insert(rope.tail_pos);

    for line in input.lines() {
        let mut chars = line.split_whitespace();

        let char_move = chars.next().unwrap();
        let amount = chars.next().unwrap().parse().unwrap();

        for _ in 0..amount {
            match char_move {
                "U" => rope.move_up(),
                "D" => rope.move_down(),
                "L" => rope.move_left(),
                "R" => rope.move_right(),
                _ => panic!("wtf stowy ?"),
            }
            unique_pos.insert(rope.tail_pos);
        }
    }

    unique_pos.len()
}

pub struct Rope {
    knots: Vec<IVec2>,
}

impl Rope {
    pub fn new(size: usize) -> Self {
        Self {
            knots: vec![IVec2::ZERO; size],
        }
    }

    pub fn tail_pos(&self) -> IVec2 {
        *self.knots.last().unwrap_or(&IVec2::ZERO)
    }

    pub fn move_one(front: IVec2, back: &mut IVec2) {
        let diff = front - *back;

        match (diff.x.abs() > 1, diff.y.abs() > 1) {
            (true, true) => *back += IVec2::new(diff.x.signum(), diff.y.signum()),
            (true, false) => *back = IVec2::new(back.x + diff.x.signum(), front.y),
            (false, true) => *back = IVec2::new(front.x, back.y + diff.y.signum()),
            _ => {}
        };
    }

    pub fn update_tail(&mut self) {
        for i in 0..self.knots.len() - 1 {
            Rope::move_one(self.knots[i], &mut self.knots[i + 1]);
        }
    }

    pub fn move_up(&mut self) {
        self.knots[0] += IVec2::Y;
        self.update_tail();
    }

    pub fn move_down(&mut self) {
        self.knots[0] -= IVec2::Y;
        self.update_tail();
    }

    pub fn move_right(&mut self) {
        self.knots[0] += IVec2::X;
        self.update_tail();
    }

    pub fn move_left(&mut self) {
        self.knots[0] -= IVec2::X;
        self.update_tail();
    }
}

#[aoc(day9, part2)]
pub fn part2(input: &str) -> usize {
    let mut rope = Rope::new(10);
    let mut unique_pos = HashSet::new();
    unique_pos.insert(rope.tail_pos());

    for line in input.lines() {
        let mut chars = line.split_whitespace();

        let char_move = chars.next().unwrap();
        let amount = chars.next().unwrap().parse().unwrap();

        for _ in 0..amount {
            match char_move {
                "U" => rope.move_up(),
                "D" => rope.move_down(),
                "L" => rope.move_left(),
                "R" => rope.move_right(),
                _ => panic!("wtf stowy ?"),
            }
            unique_pos.insert(rope.tail_pos());
        }
    }

    unique_pos.len()
}
