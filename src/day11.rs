use itertools::Itertools;
use num_bigint::BigInt;

#[derive(Debug)]
pub enum Operation {
    None,
    Add(Option<BigInt>),
    Multiply(Option<BigInt>),
}

#[derive(Debug)]
pub struct Monkey {
    pub items: Vec<BigInt>,
    pub operation: Operation,
    pub test_div: BigInt,
    pub true_monkey_id: usize,
    pub false_monkey_id: usize,
    pub inspection_count: usize,
}

impl Monkey {
    pub fn new(
        items: Vec<BigInt>,
        operation: Operation,
        test_div: BigInt,
        true_monkey_id: usize,
        false_monkey_id: usize,
    ) -> Monkey {
        Monkey {
            items,
            operation,
            test_div,
            true_monkey_id,
            false_monkey_id,
            inspection_count: 0,
        }
    }

    pub fn inspect(&self) -> (usize, BigInt) {
        let mut item = self.items[0].clone();
        match &self.operation {
            Operation::Add(val) => match val {
                Some(val) => item += val,
                None => item += item.clone(),
            },
            Operation::Multiply(val) => match val {
                Some(val) => item *= val,
                None => item *= item.clone(),
            },
            _ => {}
        }

        item /= BigInt::from(3);

        let id = if item.clone() % self.test_div.clone() == BigInt::from(0) {
            self.true_monkey_id
        } else {
            self.false_monkey_id
        };

        (id, item)
    }

    pub fn inspect_part2(&self, lcm: BigInt) -> (usize, BigInt) {
        let mut item = self.items[0].clone();
        match &self.operation {
            Operation::Add(val) => match val {
                Some(val) => item += val,
                None => item += item.clone(),
            },
            Operation::Multiply(val) => match val {
                Some(val) => item *= val,
                None => item *= item.clone(),
            },
            _ => {}
        }

        item %= lcm;

        let id = if item.clone() % self.test_div.clone() == BigInt::from(0) {
            self.true_monkey_id
        } else {
            self.false_monkey_id
        };

        (id, item)
    }
}

const ROUND_AMMOUNT: i32 = 20;
const ROUND_AMMOUNT_PART_2: i32 = 10_000;

#[aoc(day11, part1)]
pub fn part1(input: &str) -> usize {
    let mut monkeys: Vec<Monkey> = vec![];

    let mut lines = input.lines();

    while let Some(_) = lines.next() {
        let items_line = lines.next().unwrap();
        let mut splitted_items_line = items_line.split_whitespace();
        splitted_items_line.next();
        splitted_items_line.next();
        let nums_string: String = splitted_items_line.collect();
        let starting_items: Vec<BigInt> = nums_string
            .split(',')
            .map(|num| num.parse().unwrap())
            .collect();

        let mut operation_str = lines.next().unwrap().split_whitespace();
        operation_str.next();
        operation_str.next();
        operation_str.next();
        operation_str.next();

        let operator = operation_str.next().unwrap();
        let value_str = operation_str.next().unwrap();
        let value = match value_str {
            "old" => None,
            _ => Some(value_str.parse().unwrap()),
        };

        let operation = match operator {
            "*" => Operation::Multiply(value),
            "+" => Operation::Add(value),
            _ => panic!("Bad operator"),
        };

        let divisible: BigInt = lines
            .next()
            .unwrap()
            .split_whitespace()
            .last()
            .unwrap()
            .parse()
            .unwrap();

        let monkey_true: usize = lines
            .next()
            .unwrap()
            .split_whitespace()
            .last()
            .unwrap()
            .parse()
            .unwrap();

        let monkey_false: usize = lines
            .next()
            .unwrap()
            .split_whitespace()
            .last()
            .unwrap()
            .parse()
            .unwrap();
        lines.next();

        let monkey = Monkey::new(
            starting_items,
            operation,
            divisible,
            monkey_true,
            monkey_false,
        );

        monkeys.push(monkey);
    }

    for _ in 0..ROUND_AMMOUNT {
        for i in 0..monkeys.len() {
            for _ in 0..monkeys[i].items.len() {
                let (index, item) = monkeys[i].inspect();
                monkeys[i].items.remove(0);
                monkeys[i].inspection_count += 1;
                monkeys[index].items.push(item);
            }
        }
    }

    let sorted: Vec<usize> = monkeys
        .iter()
        .map(|monkey| monkey.inspection_count)
        .sorted()
        .rev()
        .collect();

    dbg!(sorted)
        .into_iter()
        .take(2)
        .reduce(|accum, num| accum * num)
        .unwrap()
}

#[aoc(day11, part2)]
pub fn part2(input: &str) -> usize {
    let mut monkeys: Vec<Monkey> = vec![];
    // I cheated for the lcm : https://www.youtube.com/watch?v=W9vVJ8gDxj4
    let mut lcm = BigInt::from(1);

    let mut lines = input.lines();

    while let Some(_) = lines.next() {
        let items_line = lines.next().unwrap();
        let mut splitted_items_line = items_line.split_whitespace();
        splitted_items_line.next();
        splitted_items_line.next();
        let nums_string: String = splitted_items_line.collect();
        let starting_items: Vec<BigInt> = nums_string
            .split(',')
            .map(|num| num.parse().unwrap())
            .collect();

        let mut operation_str = lines.next().unwrap().split_whitespace();
        operation_str.next();
        operation_str.next();
        operation_str.next();
        operation_str.next();

        let operator = operation_str.next().unwrap();
        let value_str = operation_str.next().unwrap();
        let value = match value_str {
            "old" => None,
            _ => Some(value_str.parse().unwrap()),
        };

        let operation = match operator {
            "*" => Operation::Multiply(value),
            "+" => Operation::Add(value),
            _ => panic!("Bad operator"),
        };

        let divisible: BigInt = lines
            .next()
            .unwrap()
            .split_whitespace()
            .last()
            .unwrap()
            .parse()
            .unwrap();

        let monkey_true: usize = lines
            .next()
            .unwrap()
            .split_whitespace()
            .last()
            .unwrap()
            .parse()
            .unwrap();

        let monkey_false: usize = lines
            .next()
            .unwrap()
            .split_whitespace()
            .last()
            .unwrap()
            .parse()
            .unwrap();
        lines.next();

        let monkey = Monkey::new(
            starting_items,
            operation,
            divisible,
            monkey_true,
            monkey_false,
        );

        monkeys.push(monkey);
    }

    for monkey in &monkeys {
        lcm *= lcm.clone() * monkey.test_div.clone();
    }

    // dbg!(lcm);

    for _ in 0..ROUND_AMMOUNT_PART_2 {
        for i in 0..monkeys.len() {
            for _ in 0..monkeys[i].items.len() {
                let (index, item) = monkeys[i].inspect_part2(lcm.clone());

                monkeys[i].items.remove(0);
                monkeys[i].inspection_count += 1;
                monkeys[index].items.push(item);
            }
        }
    }

    let sorted: Vec<usize> = monkeys
        .iter()
        .map(|monkey| monkey.inspection_count)
        .sorted()
        .rev()
        .collect();

    sorted
        .into_iter()
        .take(2)
        .reduce(|accum, num| accum * num)
        .unwrap()
}
